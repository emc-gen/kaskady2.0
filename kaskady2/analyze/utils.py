"""Misc utils for fits"""
import typing

import numpy as np
from scipy.stats import chisqprob


__all__ = [
  "ChisquareTestResult", "chisquare_test"
]


ChisquareTestResult = typing.NamedTuple("ChisquareTestResult", [
  ("prob", float),
  ("chisq_sum", float),
  ("sum_elems", np.ndarray)
])


def chisquare_test(
    data: np.ndarray, fitted: np.ndarray, sigma: np.ndarray, freedom_degrees: int
  ) -> ChisquareTestResult:
  """
  :param data: measured data
  :param fitted: fitted data
  :param sigma: fitted data uncertainity
  :param freedom_degrees: additional freedom degrees
  :return: tuple containing probability and reduced chisquare sum

  >>> chisquare_test(np.ones(10), np.ones(10), np.ones(10), 1)
  ChisquareTestResult(prob=1.0, chisq_sum=0.0, ...)

  >>> chisquare_test(.5*np.ones(10), np.ones(10), np.ones(10), 1)
  ChisquareTestResult(prob=0.9617..., chisq_sum=0.31..., ...)

  >>> data = np.asarray([60, 88, 39, 13], dtype=float)
  >>> expected = np.asarray([80.4, 80.4, 32.2, 7.0])

  Taken from: Taylor: "Introduction to Error Analysis", it would be stupid to make error just here!

  >>> chisquare_test(expected, data, np.sqrt(expected), 0)
  ChisquareTestResult(prob=0.00592..., chisq_sum=4.157..., ...)

  >>> chisquare_test(np.ones(10), np.ones(10), np.zeros(10), 1)  # doctest: +IGNORE_EXCEPTION_DETAIL
  Traceback (most recent call last):
  AssertionError
  >>> chisquare_test(np.ones(10), np.ones(9), np.ones(10), 1)  # doctest: +IGNORE_EXCEPTION_DETAIL
  Traceback (most recent call last):
  AssertionError
  >>> chisquare_test(np.ones(9), np.ones(10), np.ones(10), 1)  # doctest: +IGNORE_EXCEPTION_DETAIL
  Traceback (most recent call last):
  AssertionError
  >>> chisquare_test(np.ones(10), np.ones(10), np.ones(9), 1)   # doctest: +IGNORE_EXCEPTION_DETAIL
  Traceback (most recent call last):
  AssertionError
  """

  assert data.shape == fitted.shape
  assert data.shape == sigma.shape
  assert np.all(sigma != 0)

  elems = np.power((data - fitted) / sigma, 2)
  chsq_sum = np.sum(elems)

  dof = len(sigma) - freedom_degrees - 1
  return ChisquareTestResult(chisqprob(chsq_sum, dof), chsq_sum / dof, elems)
