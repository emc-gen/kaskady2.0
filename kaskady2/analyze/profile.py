"""Package for reading profiles"""
import abc
import math
import pathlib
import typing

import numpy as np

from kaskady2.utils import make_readonly_array

__all__ = [
  "LongitudinalProfile",
  "GeometryCoordinate",
  "Geometry",
  "RadialGeometry"
]


_LongitudinalProfile = typing.NamedTuple("LongitudinalProfile", [
  ["geom_radlen", np.ndarray],
  ["deposition_mev", np.ndarray],
  ["deposition_sigma_mev", typing.Optional[np.ndarray]]
])
"""
Base class for longitudinal profile
"""


class LongitudinalProfile(_LongitudinalProfile):  # pylint: disable=too-few-public-methods
  """A Longitudinal profile that checks if arrays are not writable."""

  @staticmethod
  def from_rw_array(geom_radlen, deposition_mev, deposition_sigma_mev):
    """
    Creates instance of this class and allows parameters to be writeable
    arrays (__new__ asserts that they are read only)
    :see:`__reduce__` for explanation why it is needed.
    """

    return LongitudinalProfile(
      geom_radlen=make_readonly_array(geom_radlen, make_view=True),
      deposition_mev=make_readonly_array(deposition_mev, make_view=True),
      deposition_sigma_mev=make_readonly_array(deposition_sigma_mev, make_view=True)
    )

  def __new__(cls, geom_radlen, deposition_mev, deposition_sigma_mev):
    assert len(geom_radlen.shape) == 1
    assert geom_radlen.shape == deposition_mev.shape
    assert (
      deposition_sigma_mev is None or
      geom_radlen.shape == deposition_sigma_mev.shape
    )
    assert geom_radlen.flags.writeable is False
    assert deposition_mev.flags.writeable is False
    assert (
      deposition_sigma_mev is None or
      deposition_sigma_mev.flags.writeable is False
    )
    return _LongitudinalProfile.__new__(
      cls, geom_radlen, deposition_mev, deposition_sigma_mev
    )

  def __reduce__(self):
    """
    This is for pickle. Unpickled arrays are writeable (despite originals being read-only)
    so his wrapper uses a constructor that allows for rw numpy arrays.
    :return:
    """
    return (
      LongitudinalProfile.from_rw_array,
      tuple(getattr(self, field) for field in self._fields)
    )


GeometryCoordinate = typing.NamedTuple("GeometryCoordinate", [
  ['count', int],
  ['width_radlen', float]
])
"""
Count represents number of bins, width represents width of single
bin in radiation length.
"""


class Geometry(object, metaclass=abc.ABCMeta):
  """
  Abstract class representing a geometry.
  """

  @property
  @abc.abstractmethod
  def incident_count(self):
    """
    :return: Number of cascades stored in this geometry.
    """

  @abc.abstractmethod
  def get_profile_coordinates(self) -> np.ndarray:
    """Returns postions of center of each bin"""
    raise NotImplementedError()

  @abc.abstractmethod
  def get_longitudinal_profile(self, incident: int) -> LongitudinalProfile:
    """
    Returns Energy deopsition in each bin for an incident. Incident is
    :param incident:
    :return:
    """
    raise NotImplementedError()

  @abc.abstractmethod
  def get_longitudinal_average_profile(self) -> LongitudinalProfile:
    """
    Return energu depositions averaged for each cascade in the set.
    """
    raise NotImplementedError()


class RadialGeometry(Geometry):
  """
  Longitudinal profile read from file where deopsitions are stored in radial
  geometry (there are other possible geometries).
  """

  @classmethod
  def from_file(cls, file_name: pathlib.Path, **kwargs):
    """
    Loads this geometry from a binary file loaded using memmap call.
    :param kwargs: kwargs are passed to __init__
    """
    array = cls.__open_file(file_name, **kwargs)
    return cls(array=array, **kwargs)

  @classmethod
  def __open_file(
      cls, file_name: typing.Union[str, pathlib.Path],
      incidents: int, slices: GeometryCoordinate, cylinders: GeometryCoordinate
  ) -> np.ndarray:
    filesize_bytes = file_name.stat().st_size
    size_per_incident = slices.count * cylinders.count * np.float64().itemsize
    expected_size_bytes = size_per_incident * incidents
    assert filesize_bytes == expected_size_bytes
    return np.memmap(
      str(file_name), dtype=np.float64, shape=(incidents, cylinders.count, slices.count),
      mode='readonly'
    )

  @property
  def incident_count(self):
    """Returns number of incidents"""
    return self.incidents

  def __init__(
      self, array: np.ndarray, incidents: int, slices: GeometryCoordinate,
      cylinders: GeometryCoordinate
  ):
    self.incidents = incidents
    self.slices = slices
    self.cylinders = cylinders
    self.array = array
    assert self.array.flags.writeable is False
    self.__validate_array()
    self.__validate_shape()

  def _expected_shape(self) -> typing.Tuple[int, int, int]:
    return (self.incidents, self.cylinders.count, self.slices.count)

  def __validate_array(self):
    assert self.array.shape == self._expected_shape()

  def __validate_shape(self):
    assert self.cylinders.count == 1, \
      "Geometries with more than a single cylinder are not supported right now"
    # NOTE: Please check that profile sigmas are sa they should be in case od these geometries.

  def get_profiles_as_array(self) -> np.ndarray:
    """
    Returns profiles as array. This array is two-dimensional (even if  underlying
    data has more complicated geometry).

    Array is indexed in such way that: ``array[cascade][bin]`` returns deposition in
    particular bin for a particular cascade.
    """
    assert self.cylinders.count == 1
    result = self.array.reshape((self.incidents, self.slices.count))
    result.setflags(write=False)
    assert result.flags.writeable is False
    return result

  def get_profile_coordinates(self) -> np.ndarray:
    slices = self.slices
    return make_readonly_array(
      np.arange(0, slices.count, 1) * slices.width_radlen + slices.width_radlen * 0.5
    )

  def get_longitudinal_average_profile(self) -> LongitudinalProfile:

    return LongitudinalProfile(
      geom_radlen=self.get_profile_coordinates(),
      deposition_mev=make_readonly_array(self.array.sum((0, 1)) / self.incidents),
      deposition_sigma_mev=make_readonly_array(
        np.std(self.array.sum(1), 0) / math.sqrt(self.incidents))
    )

  def get_longitudinal_profile(self, incident: int) -> LongitudinalProfile:
    return LongitudinalProfile(
      geom_radlen=self.get_profile_coordinates(),
      deposition_mev=make_readonly_array(self.array[incident].sum(0)),
      deposition_sigma_mev=None
    )
