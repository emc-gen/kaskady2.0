"""Various helpers usefull for ipyrhon"""

# No coverage tracked on this file
# pylint: disable=missing-docstring

import typing

import numpy as np
import pandas

from kaskady2.analyze.utils import ChisquareTestResult, chisquare_test
from .api import FitFunction, FitData, FitObject

__all__ = [
  "FitResultsDescription",
  "FitPerformer",
  "FitSetManager"
]

FitResultsDescription = typing.NamedTuple(
  "FitResultsDescription",
  (
    ("context", typing.Any),
    ("fit_ok", bool),  # True if fit was succesfull
    # True if sigma is a value of absolute error of ydata. If False there is no chisquared
    ("absolute_sigma", bool),
    ("params", typing.Optional[np.ndarray]),
    ("params_stddev", typing.Optional[np.ndarray]),
    ("fit_data", FitData),
    ("fitted_ydata", typing.Optional[np.ndarray]),
    ("param_names", typing.List[str]),
    ("chisquared", typing.Optional[ChisquareTestResult]),
  )
)


class FitPerformer(object):
  """
  A utility to perform a fit, and return useful data from it.
  """

  def __init__(self, context):
    self.context = context

  def get_fit_data(self) -> FitData:
    """Obtains fit data"""
    raise NotImplementedError

  def get_fit_function(self) -> FitFunction:
    """Obtains fit function"""
    raise NotImplementedError

  def get_context_object(self) -> typing.Any:
    """Optional context"""
    return self.context

  def do_fit(self) -> FitResultsDescription:
    """Perform the fit"""
    data = self.get_fit_data()
    function = self.get_fit_function()
    fit_obj = FitObject(data, function)
    try:
      fit_result = fit_obj.do_fit()
    except RuntimeError:
      return FitResultsDescription(
        context=self.get_context_object(),
        absolute_sigma=None,
        fit_data=data,
        param_names=function.get_param_names(),
        fit_ok=False,
        params=None,
        params_stddev=None,
        fitted_ydata=None,
        chisquared=None
      )
    except Exception as e:
      raise ValueError(self.get_context_object()) from e

    assert len(fit_result.param_values) == len(function.get_param_names())

    fitted_ydata = function.fit_function(data.xdata, *fit_result.param_values)

    sigma, absolute_sigma = data.sigma

    if absolute_sigma:
      chisquared = chisquare_test(
        data.ydata, fitted_ydata, sigma, len(fit_result.param_values)
      )
    else:
      chisquared = None

    return FitResultsDescription(
      context=self.get_context_object(),
      absolute_sigma=absolute_sigma,
      fit_data=data,
      param_names=function.get_param_names(),
      fit_ok=True,
      params=fit_result.param_values,
      params_stddev=fit_result.param_estimated_stddev,
      fitted_ydata=fitted_ydata,
      chisquared=chisquared
    )


ParameterPlotData = typing.NamedTuple(
  "ParameterPlotData",
  (
    ('parameter', str),
    ('frame', pandas.DataFrame),
    ('index_attrs', typing.Mapping[str, typing.Any]),
  )
)


def make_param_plots(
    frame: pandas.DataFrame,
    to_filter: typing.List[str],
    parameters: typing.List[str]
) -> typing.Iterable[ParameterPlotData]:
  """
  This function takes multi-indexed frame, and for each index value splits the data frame
  that resulted from indexing the frame for this value.

  Usefull for energy plots
  """
  def __param_plots_rec(filtered, to_filter, current_parameters):
    if len(to_filter) == 0:
      for parameter in parameters:
        yield ParameterPlotData(parameter, filtered, current_parameters)
      return
    current_param = to_filter[0]
    to_filter = to_filter[1:]
    for par in getattr(filtered, current_param).unique():
      pars = dict(**current_parameters)
      pars[current_param] = par
      yield from __param_plots_rec(
        filtered.loc[par],
        to_filter,
        pars
      )

  yield from __param_plots_rec(frame, to_filter, {})


class FitSetManager(object):
  """A utility to manage plots that show parameter dependence on energy"""

  @classmethod
  def restore(cls, filename, **kwargs):
    """Loads previously stored object"""
    item = cls(**kwargs)
    item.frame = pandas.read_pickle(filename)
    return item

  def __init__(self, index_attrs, context_attrs_to_extract=None, expected_params=None):
    self.frame = None
    self.pandas_data = []
    if context_attrs_to_extract is None:
      context_attrs_to_extract = {
        'material': 'material.name_pretty',
        'ecuts_kev': 'ecuts.energy_kev',
        'energy_mev': 'run_info.energy_mev',
      }
    self.context_attrs_to_extract = context_attrs_to_extract
    self.index_attrs = index_attrs
    self.expected_params = expected_params

  def save(self, filename):
    """Saves pandas frame to a file"""
    pandas.to_pickle(self.frame, filename)

  def add_results(self, results: typing.Iterable[FitResultsDescription]):
    self.pandas_data.extend((
      self.extract_result(result)
      for result in results
    ))

  def make_frame(self, keep_data=False):
    table = pandas.DataFrame(data=self.pandas_data)
    index_attrs = list(self.index_attrs)  # pandas is picky about types
    if not keep_data:
      del self.pandas_data
    self.frame = table.sort(index_attrs).set_index(drop=False, keys=index_attrs)

  def extract_result(self, result: FitResultsDescription) -> dict:
    fit_ok = result.fit_ok
    has_sigma = result.fit_ok and result.absolute_sigma
    result_dict = {
      "fit_ok": fit_ok,
      "chisq sum": result.chisquared.chisq_sum if has_sigma else None,
      "chisq prob": result.chisquared.prob if has_sigma else None,
    }
    params = result.params
    params_std = result.params_stddev
    assert self.expected_params is None or result.param_names == self.expected_params
    if fit_ok:
      for idx, param in enumerate(result.param_names):
        result_dict[param] = params[idx]
        result_dict["sigma " + param] = params_std[idx]
    context = self.extract_context(result.context)
    # some entries would shadow other, which is not good
    assert len(set(context.keys()).intersection(set(result_dict.keys()))) == 0
    result_dict.update(context)
    return result_dict

  def make_param_plots(self, *, parameters, index_attrs: typing.Iterable[str]=None):
    if index_attrs is None:
      index_attrs = self.index_attrs

    return make_param_plots(self.frame, index_attrs, parameters)

  @staticmethod
  def __get_dotted_attr(obj, attr_name):
    parts = attr_name.split(".")
    for part in parts:
      obj = getattr(obj, part)
    return obj

  def extract_context(self, context: typing.Any) -> dict:
    return {
      name: self.__get_dotted_attr(context, path)
      for name, path in self.context_attrs_to_extract.items()
    }

