"""Fit api"""

import abc
import typing

import numpy as np
from scipy.optimize import curve_fit

__all__ = [
  "ParamEstimations",
  "FitFunction",
  "FitData",
  "FitObject"
]

ParamEstimations = typing.NamedTuple(
  "ParamEstimations",
  [
    ('params', typing.List[float]),
    ('bounds', typing.Optional[typing.List[typing.Tuple[float, float]]])
    # Bounds are either None (no bounds) or a list of bounds for each parameter in form of:
    # (lower, upper)
  ]
)


class InvalidInitialParams(Exception):
  """Errors raised when initial params are wrong"""
  pass


class FitFunction(object, metaclass=abc.ABCMeta):
  """
  Abstract class represents a fit function, fit function has two operations:

  1. Evaluate the function for given argument and parameters
  2. Estimate initial parameters

  This way doing fits can be completely automated.
  """

  @abc.abstractmethod
  def get_param_names(self) -> typing.List[str]:
    """
    Returns list of parameter names.
    """
    raise NotImplementedError

  @abc.abstractmethod
  def fit_function(self, x: np.ndarray, *pars: float) -> np.ndarray:
    """Evaluates the function for each x, with *pars as parameters """
    raise NotImplementedError

  @abc.abstractmethod
  def _estimate_initial_parameters(self, xdata: np.ndarray, ydata: np.ndarray) -> ParamEstimations:
    """Estimate initial parameters from xdata and ydata."""
    raise NotImplementedError

  def estimate_initial_parameters(self, xdata: np.ndarray, ydata: np.ndarray) -> ParamEstimations:
    """
    Estimate initial parameters from xdata and ydata. This method verify parameter
    estimations producing a easy to read errors.
    """
    estimations = self._estimate_initial_parameters(xdata, ydata)
    param_names = self.get_param_names()
    if estimations.bounds is None:
      # No bounds no need to check them
      return estimations
    assert len(param_names) == len(estimations.params)
    assert len(param_names) == len(estimations.params)

    errors = []

    for name, param, (lower, upper) in zip(param_names, estimations.params, estimations.bounds):
      if lower > param or upper < param:
        errors.append(
          "Parameter {name}  (value: {value}) is outside of its bounds ({lower}, {upper})".format(
            name=name, value=param, lower=lower, upper=upper
          )
        )
    if errors:
      raise InvalidInitialParams("\n".join(errors))
    return estimations


class FitData(object, metaclass=abc.ABCMeta):
  """Data on which fit will be performed."""

  @property
  @abc.abstractmethod
  def xdata(self) -> np.ndarray:
    """Function arguments"""
    raise NotImplementedError

  @property
  @abc.abstractmethod
  def ydata(self) -> np.ndarray:
    """Expected values"""
    raise NotImplementedError

  @property
  @abc.abstractmethod
  def sigma(self) -> typing.Tuple[np.ndarray, bool]:
    """
    :return: (sigma, absolute_sigma) absolute_sigma is passed to absolute_sigma curve_fit call
    """
    raise NotImplementedError


FitResults = typing.NamedTuple(
  'FitResults',
  (
    ("param_values", np.ndarray),
    ("param_covariance", np.ndarray),
    ("param_estimated_stddev", np.ndarray),
  )
)


class FitObject(object, metaclass=abc.ABCMeta):  # pylint: disable=too-few-public-methods
  """
  Object that performs fit using FitData and FitFunction.
  To perform the fit we use curve_fit, and nothing more.
  """

  def __init__(self, data: FitData, function: FitFunction):
    super().__init__()
    self.data = data
    self.function = function

  def do_fit(self) -> FitResults:
    """Actually perform the fit."""
    geom = self.data.xdata
    profile = self.data.ydata
    sigma, absolute_sigma = self.data.sigma

    assert len(geom.shape) == 1
    assert len(profile.shape) == 1
    assert len(sigma.shape) == 1
    assert geom.shape == profile.shape
    assert geom.shape == sigma.shape
    assert isinstance(absolute_sigma, bool)
    assert np.all(sigma != 0)

    params, param_bounds = self.function.estimate_initial_parameters(
      geom, profile
    )

    if param_bounds is None:
      cf_bounds = (-np.inf, np.inf)
    else:            # scipy wants this redefined
      cf_bounds = [  # pylint: disable=redefined-variable-type
        [p[0] for p in param_bounds],
        [p[1] for p in param_bounds],
      ]

    params, p_cov = curve_fit(
      f=self.function.fit_function,
      xdata=geom,
      ydata=profile,
      sigma=sigma,
      absolute_sigma=absolute_sigma,
      p0=params,
      bounds=cf_bounds,
      method='dogbox'
    )
    return FitResults(
      param_values=params,
      param_covariance=p_cov,
      param_estimated_stddev=np.sqrt(np.abs(np.diagonal(p_cov)))
    )


