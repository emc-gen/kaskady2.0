"""One way to calculate fluctuation"""
# pylint: disable=wildcard-import

from . calculate import *
from .fits import *
