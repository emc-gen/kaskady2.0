"""Fits for the this kind of fluctuation"""

import typing

import numpy as np
from scipy import signal

from kaskady2 import utils
from kaskady2.analyze.fits import FitData, FitFunction, ParamEstimations
from .calculate import FluctuationCalculation

__all__ = [
  "WeibullFunc",
  "FluctuationFitData",
]


class FluctuationFitData(FitData):
  """"
  Formats fluctuation data so it fits to out fit api.
  """

  def __init__(self, calc: FluctuationCalculation):
    super().__init__()
    self.__ydata, bins = calc.make_histogram(normed=True)
    # xdata is bin middles.
    self.__xdata = 0.5 * (bins[1:] + bins[:-1])
    self.__sigma = utils.fix_sigma(np.sqrt(self.__ydata)), False

  @property
  def sigma(self) -> typing.Tuple[np.ndarray, bool]:
    return self.__sigma

  @property
  def ydata(self) -> np.ndarray:
    return self.__ydata

  @property
  def xdata(self) -> np.ndarray:
    return self.__xdata


class WeibullFunc(FitFunction):

  """Slightly modified version of Weibull distribution"""

  def get_param_names(self) -> typing.List[str]:
    return ['lambda', "k", "b"]

  def fit_function(self, x: np.ndarray, *pars: float) -> np.ndarray:
    lambda_par, k, b = pars
    x = (x - b)
    result = np.where(
      x < 0,
      0,
      (k / lambda_par) * (x / lambda_par) ** (k - 1) * np.exp(-(x / lambda_par)**k)
    )
    distro_sum = result.sum()
    if distro_sum == 0:
      return result  # pragma: no cover
    return result / distro_sum

  def _estimate_initial_parameters(self, xdata: np.ndarray, ydata: np.ndarray) -> ParamEstimations:
    """
    This doesn't converge mostly when there is no overlap between initial weibull func and data.
    So we estimate initial parameters by:

    1. Assuming k=5
    2. Setting b parameter (that moves x) to minimal of xdata (this is where histogram starts)
    3. Comparing FWHM of weibull and histogram and setting lambda so they match.
    """
    lambda_par, k, b = 1, 5, +np.min(xdata)

    histo_fwhm = utils.get_fwhm(xdata=xdata, ydata=signal.medfilt(ydata, kernel_size=5))
    fit_fwhm = utils.get_fwhm(
      ydata=self.fit_function(xdata, lambda_par, k, b),
      xdata=xdata
    )

    lambda_par = histo_fwhm / fit_fwhm

    return ParamEstimations(
      params=[lambda_par, k, b],
      bounds=[
        (1E-3, np.inf),
        (1E-3, np.inf),
        (-np.inf, np.inf),
      ]
    )
