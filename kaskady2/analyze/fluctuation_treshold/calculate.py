"""
Calculates fluctuations using threshold method.

We define a threshold A, and for each cascade we calculate at which position
this threshold was reached.
Then we histogram these positions.
"""

import typing

import numpy as np
from cached_property import cached_property
from scipy import signal

from kaskady2.analyze.profile import RadialGeometry

__all__ = [
  "FitStats", "FluctuationCalculation"
]


FitStats = typing.NamedTuple(
  "FitStats", [
    ("mean", float),
    ("max_bin", int),
    ("fwhm_bin", int)
  ]
)


class FluctuationCalculation(object):
  """
  For method see file docstring above.
  """

  @classmethod
  def from_geometry(cls, geometry: RadialGeometry, threshold: float) -> 'FluctuationCalculation':
    """Creates calculation object from RadialGeometry."""
    return cls(geometry.get_profiles_as_array(), threshold, geometry.slices.width_radlen)

  def __init__(self, array: np.ndarray, threshold: float, bin_width: float=1.0):
    """
    :param  array: A 2d array containing energy depositions, array[ii][jj] should return deposition
                  in ii-th cascade in jj-th bin.
                  Size of this array is N x M.
    :param threshold: See module docstring
    :param xdata: Positions of slices
    """

    self.__threshold = threshold
    assert threshold > 0, "Threshold needs to be more than 0"
    assert threshold < 1, "Threshold needs to be less than 1"
    self.array = array
    self.bin_width = bin_width
    assert len(array.shape) == 2

  @cached_property
  def _cumsum(self) -> np.ndarray:
    """
    Cumulative sum of depositions in ``self.array``. Shape is N x M.
    """
    return np.cumsum(self.array, 1)

  @cached_property
  def _thresholds(self) -> np.ndarray:
    """
    Energy thresholds, for each cascade, correspond to self.threshold times
    total deposition.

    Shape is N x 1
    """
    totals = self._cumsum[:, -1]
    return (self.__threshold * totals)[:, np.newaxis]

  @cached_property
  def _lower_than_threshold(self) -> np.ndarray:
    """
    Boolean array where 1 means that cumsum in given bin is <=
    then threshold.
    """
    return self._cumsum <= self._thresholds

  @cached_property
  def raw_fluctuation(self) -> typing.Sequence[int]:
    """
    An array that for each cascade contains index of bin
    in which threshold was reached.
    """
    return np.add.accumulate(self._lower_than_threshold, axis=1)[:, -1]

  def _get_max_bin(self, histogram, bin_middles):  # pylint: disable=no-self-use
    hist_max_index = np.argmax(histogram)
    return bin_middles[hist_max_index]

  @cached_property
  def histogram_stats(self) -> FitStats:
    """
    Returns various stats of the fit
    """
    # create a spline of x and blue-np.max(blue)/2
    hist, bins = self.make_histogram(filter_size=5)
    bin_middles = (bins[:-1] + bins[1:]) / 2
    return FitStats(
      # utils.get_fwhm(hist, bin_middles), Find more robust method for estimating fwhm see:
      # https://gitlab.com/emc-gen/kaskady2.0/issues/1
      fwhm_bin=None,
      max_bin=self._get_max_bin(hist, bin_middles),
      mean=self.expected_bin
    )

  @cached_property
  def histogram(self):
    """Shortcut for self.make_histogram()"""
    return self.make_histogram()

  @cached_property
  def expected_bin(self) -> float:
    """
    Returns expected value for threshold. This method uses
    man as mean is more resistant to outilers (that will be present
    in the dataset).
    """
    return np.mean(self.raw_fluctuation)

  def percentile_bin(
      self,
      percentile: typing.Union[None, float, typing.Tuple[float]]=tuple(range(5, 100, 10))
    ):
    """
    :param percentile: Float, or a tuple of floats. Each value needs to be
                       in range [0, 100]. This function will return array containing
                       calculated percentiles for each percentile in this array
                       (or a single element array if a float was passed).

                       Defaults to: ``tuple(range(5, 100, 10))``.
    :return:
    """
    return np.percentile(self.raw_fluctuation, percentile)

  def make_histogram(
      self, *, filter_size=0, normed=True, **kwargs
  ) -> typing.Tuple[np.ndarray, np.ndarray]:
    """
    Returns histogram of channels.
    :param filter_size: By default zero -- -that disables filtering, any other value will
                        pass histogram by `signal.medfilt` method.
    :param normed: By default True. If true will norm data to one.
    :param kwargs: passed to np.histogram.
    :return:
    """
    if 'bins' not in kwargs:
      left = np.min(self.raw_fluctuation)
      right = np.max(self.raw_fluctuation)
      kwargs['bins'] = np.arange(left, right + 1, 1) * self.bin_width
    # Instead of density you should use normed.
    assert 'density' not in kwargs
    # Rescale histogram to use radlen units
    hist, bins = np.histogram(self.raw_fluctuation * self.bin_width, **kwargs)
    if normed:
      hist = hist / hist.sum()
    if filter_size > 0:
      hist = signal.medfilt(hist, [filter_size])
    return hist, bins

