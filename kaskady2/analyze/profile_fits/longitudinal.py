"""Fits to longitudinal profiles"""

import typing

import numpy as np

from kaskady2.analyze.fits import FitData, FitFunction, ParamEstimations
from kaskady2.analyze.profile import LongitudinalProfile
from kaskady2.utils import fix_sigma

__all__ = [
  "profile_fit_function_gold",
  "SimpleFitData",
  "FitDataWrapper",
  "LongitudinalProfileFitData",
  "LongitudinalProfileFitDataProportionalSigma",
  "LongitudinalProfileFitFunction"
]


def profile_fit_function_gold(norm: float, args: typing.Iterable[float], x: np.ndarray):
  """
  Postulated function that should fit empirical data.
  """

  beta, delta, gamma, epsilon = args

  xprim = x + epsilon
  xprim[xprim <= 0] = 0

  expected = norm * xprim ** beta * np.exp(-gamma * x ** delta)

  return expected


class SimpleFitData(FitData):
  """Plain Python object implementing Fit Data Interface"""
  def __init__(
      self, xdata: np.ndarray, ydata: np.ndarray, sigma: typing.Tuple[np.ndarray, bool]
  ):
    super().__init__()

    assert len(xdata.shape) == 1
    assert xdata.shape == ydata.shape
    assert xdata.shape == sigma[0].shape
    assert isinstance(sigma[1], bool)
    assert np.all(sigma[0] != 0)

    self.__xdata = xdata
    self.__ydata = ydata
    self.__sigma = sigma

  @property
  def xdata(self) -> np.ndarray:
    return self.__xdata

  @property
  def sigma(self) -> typing.Tuple[np.ndarray, bool]:
    return self.__sigma

  @property
  def ydata(self) -> np.ndarray:
    return self.__ydata


class FitDataWrapper(FitData):
  """A wrapper around fit data that allows for some modifications."""
  def __init__(
      self, base: FitData,
      fit_to_bin: int=None,
      fit_until_deposition_reaches: float=None,
      normalize_sum_to: float=None
  ):
    super().__init__()
    self.base = base
    self.fit_to_bin = None
    self.ydata_norm = None
    self.__apply_x_cuts(fit_to_bin, fit_until_deposition_reaches)
    self.__apply_y_normalisation(normalize_sum_to)

  def __apply_x_cuts(self, fit_to_bin, fit_until_deposition_reaches):
    assert not (fit_to_bin is not None and fit_until_deposition_reaches is not None)
    self.fit_to_bin = fit_to_bin
    if fit_until_deposition_reaches is None:
      return
    assert fit_until_deposition_reaches <= 1
    assert fit_until_deposition_reaches > 0
    ydata = self.base.ydata
    ydata_sum = np.sum(ydata)
    ydata_cumsum = np.cumsum(ydata)
    self.fit_to_bin = np.nonzero(ydata_cumsum / ydata_sum > fit_until_deposition_reaches)[0][0]

  def __apply_y_normalisation(self, normalize_sum_to):
    if normalize_sum_to is None:
      return
    ydata = self.base.ydata[:self.fit_to_bin]
    self.ydata_norm = normalize_sum_to / ydata.sum()

  @property
  def xdata(self) -> np.ndarray:
    if self.fit_to_bin is None:
      return self.base.xdata
    return self.base.xdata[:self.fit_to_bin]

  @property
  def sigma(self) -> typing.Tuple[np.ndarray, bool]:
    sigma, sigma_absolute = self.base.sigma
    sigma = sigma.copy()

    if self.fit_to_bin is not None:
      sigma = sigma[:self.fit_to_bin]

    if self.ydata_norm is not None:
      sigma = sigma * self.ydata_norm

    return sigma, sigma_absolute

  @property
  def ydata(self) -> np.ndarray:
    ydata = self.base.ydata.copy()
    if self.fit_to_bin is not None:
      ydata = self.base.ydata.copy()[:self.fit_to_bin]
    if self.ydata_norm is not None:
      ydata *= self.ydata_norm
    return ydata


class LongitudinalProfileFitData(FitData):
  """
  Fit data read from longitudinal profile.
  """

  def __init__(self, profile: LongitudinalProfile):
    """

    :param profile: Profile that will be fitted against
    :param fit_to_bin: If set allows one to fit only up to designed bin
    """
    super().__init__()
    self.profile = profile

  @property
  def ydata(self) -> np.ndarray:
    """Average depositions."""
    return self.profile.deposition_mev

  @property
  def xdata(self) -> np.ndarray:
    """Middele of each bit"""
    return self.profile.geom_radlen

  @property
  def sigma(self) -> typing.Tuple[np.ndarray, bool]:
    """Returns stddev"""
    return fix_sigma(self.profile.deposition_sigma_mev), True


class LongitudinalProfileFitDataProportionalSigma(LongitudinalProfileFitData):
  """Longitudinal profile with overriden sigma"""

  @property
  def sigma(self) -> typing.Tuple[np.ndarray, bool]:
    """
    Sigma for this profile is calculated using sqrt of ydata. Should not be used to calculate
    chsiquared.
    """
    return fix_sigma(0.03 * np.sqrt(self.ydata)), False


class LongitudinalProfileFitFunction(FitFunction):
  """Fit function using proposed fits"""

  def _estimate_initial_parameters(self, xdata: np.ndarray, ydata: np.ndarray) -> ParamEstimations:
    """
    Starts with sane-ish values of parameters and then recalculates norming parameter
    so sum of the profile and the fit is the same.
    """
    initial = [2, 1.1, .3, 0]
    initial_profile = self.fit_function(xdata, 1, *initial)
    ydata_sum = ydata.sum()
    initial_sum = initial_profile.sum()
    new_norm = float(ydata_sum / initial_sum)
    return ParamEstimations(
      params=[new_norm] + initial,
      bounds=[
        (0, np.inf), (0, np.inf), (0, np.inf), (0, np.inf), (-np.inf, np.inf)
      ]
    )

  def fit_function(self, x, *pars):
    """Evaluates fit function"""
    return profile_fit_function_gold(pars[0], pars[1:], x)

  def get_param_names(self) -> typing.List[str]:
    return [
      "alpha", "beta", "delta", "gamma", "epsilon"
    ]


