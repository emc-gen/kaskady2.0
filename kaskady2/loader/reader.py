"""Loads cascade metadata"""
import os
import pathlib
import typing
from functools import lru_cache

from kaskady2.analyze.profile import Geometry, RadialGeometry
from kaskady2.loader.config import CascadeSetConfig


class FileWalker(object):  # pylint: disable=too-few-public-methods
  """
    Object that does walk on a directory tree, returning
    paths representing files and doing filtering on yhem
  """

  def __init__(
      self,
      root_file: pathlib.Path,
      filter_function: typing.Callable[[pathlib.Path], bool],
      **kwargs
  ):
    self.root_file = root_file
    self.filter = filter_function
    self.kwargs = kwargs

  def __call__(self) -> typing.Iterable[pathlib.Path]:
    return self._filter_walk(os.walk(str(self.root_file), **self.kwargs))

  def _filter_walk(
      self, walk_object: typing.Iterable[tuple]
  ) -> typing.Iterable[pathlib.Path]:
    filter_func = self.filter
    for dirpath, _, filenames in walk_object:
      for filename in filenames:
        p = pathlib.Path(os.path.join(dirpath, filename))
        if p.is_file() and p.exists() and filter_func(p):
          yield p.resolve()


class CascadeSetLoader():

  """Loads a cascade set returning it's geometry."""

  MAIN_GEOMETRY_FILE = "main_geometry.dat"
  """
  Name of file containing main geometry
  """

  METADATA_FILE = "metadata.ini"
  """
  Name of file containing metadata
  """

  @classmethod
  @lru_cache()
  def _find_cascades_unfiltered(
      cls, root_dir: pathlib.Path
  ) -> typing.List['CascadeSetLoader']:
    """
    Returns list of all cascades in root_dir.
    """
    def __loader():
      file_walker = FileWalker(root_dir, lambda p: p.name == cls.METADATA_FILE)
      for path in file_walker():
        yield cls.from_file(path)

    def __get_key(loader: CascadeSetLoader):
      config = loader.config
      return (
        config.material.name,
        - config.ecuts.energy_kev,
        - config.run_info.energy_mev
      )

    return sorted(__loader(), key=__get_key)

  @classmethod
  def find_cascades(
      cls,
      root_dir: pathlib.Path,
      energies_mev: typing.Set[int]=None,
      cuts_kev: typing.Set[int]=None,
      material_names: typing.Set[str]=None
  ) -> typing.List['CascadeSetLoader']:
    """
    Finds all cascades in a root_dir. Cascade is a directory containing metadata.ini file.
    You can filter cascades by energy, cuts, and material.
    :param root_dir: Directory where to search for cascades.
    :param energies_mev:
    :param cuts_kev:
    :param material_names:
    :return:
    """
    filters = []
    if energies_mev:
      filters.append(lambda x: x.run_info.energy_mev in energies_mev)
    if cuts_kev:
      filters.append(lambda x: x.ecuts.energy_kev in cuts_kev)
    if material_names:
      filters.append(lambda x: x.material.name in material_names)

    if not filters:
      return cls._find_cascades_unfiltered(root_dir)

    def __filter_cascades():
      for cascade_loader in cls._find_cascades_unfiltered(root_dir):
        config = cascade_loader.config
        if all((f(config) for f in filters)):
          yield cascade_loader

    return list(__filter_cascades())

  @classmethod
  def from_file(cls, metadata_ini_path: pathlib.Path):
    """Creates CascadeLoader by loading an ini file"""
    return cls(
      metadata_ini_path.parent,
      CascadeSetConfig.open_path(metadata_ini_path)
    )

  def __init__(self, root_path: pathlib.Path, config: CascadeSetConfig):
    self.__config = config
    self.__root_path = root_path

  @property
  def config(self) -> CascadeSetConfig:
    """Returns configuration for CascadeSet"""
    return self.__config

  @property
  def main_geometry(self) -> Geometry:
    """Returns geometry for the cascade set"""
    assert self.config.main_geometry.enabled
    assert self.config.main_geometry.move_z_to_first_interaction
    return RadialGeometry.from_file(
      self.__root_path / self.MAIN_GEOMETRY_FILE,
      incidents=self.config.run_info.incidents,
      slices=self.config.main_geometry.zbins,
      cylinders=self.config.main_geometry.rbins
    )


