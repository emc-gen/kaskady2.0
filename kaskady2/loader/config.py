"""Loads cascade metadata"""
import configparser
import enum
import pathlib
import typing

from kaskady2.analyze.profile import GeometryCoordinate


class InitialParticle(enum.Enum):
  """Enum that represents particle that initiates the cascade."""
  GAMMA = "GAMMA"
  ELECTRON = "ELECTRON"

  @classmethod
  def from_config(cls, val: str) -> 'InitialParticle':
    """Transforms string to instance of this enum"""
    if val.lower() == cls.GAMMA.value.lower():
      return cls.GAMMA
    if val.lower() == cls.ELECTRON.value.lower():
      return cls.ELECTRON
    raise ValueError("Invalid particle")


class CutsType(enum.Enum):
  """Type of cuts"""
  G4Cuts = "G4Cuts"
  """
  Normal cuts
  """
  Custom = "Custom"
  """
  Homebrewn cuts written in simulation software.
  """


RunInfo = typing.NamedTuple("RunInfo", [
  ('particle', InitialParticle),
  ('incidents', int),
  ('energy_mev', int)
])


Material = typing.NamedTuple("Material", [
  ('name', str),
  ('name_pretty', str),
  ('w_coeff', float),
  ('density_g_cm3', float),
  ('rad_len_mm', float)
])


Cuts = typing.NamedTuple("Cuts", [
  ('type', CutsType),
  ('energy_kev', int),
  # Having energy in MeV is usefull when plotting, etc.
  ('energy_mev', str),
])


MainGeometry = typing.NamedTuple("MainGeometry", [
  ("enabled", bool),
  ("move_z_to_first_interaction", bool),
  ("zbins", GeometryCoordinate),
  ("rbins", GeometryCoordinate)
])


EventsDump = typing.NamedTuple("EventsDump", [
  ("event_count", int),
  ("structure_size", int),
  ("version", int)
])

BOOLEAN_STATES = {
  '1': True, 'yes': True, 'true': True, 'on': True,
  '0': False, 'no': False, 'false': False, 'off': False
}

MATERIAL_PRETTY_NAMES = {
  k.lower(): v for k, v in {
    "xenon": "Liquid Xe",
    "G4_Pb": "Lead (Pure)",
    "G4_BGO": "BGO",
  }.items()
}


def str_to_bool(value: str) -> bool:
  """Converts various boolean strings to proper bool"""
  return BOOLEAN_STATES[value.lower()]


class CascadeSetConfig(object):
  """
  Parsed configuration.
  """

  @classmethod
  def open_validated(cls, **kwargs) -> 'CascadeSetConfig':
    """Factory method: Reads the config parser, and validates the configuration. """
    instance = cls(**kwargs)
    instance._validate()  # pylint: disable=protected-access
    return instance

  @classmethod
  def open_path(cls, file: typing.Union[pathlib.Path, str]) -> 'CascadeSetConfig':
    """Reads config from an ini file"""
    if isinstance(file, pathlib.Path):
      file = str(file)
    config_parser = configparser.ConfigParser()
    config_parser.read(file)
    return cls.open_validated(config=config_parser)

  def __init__(self, config: typing.Mapping[str, dict]):
    self.__version = CascadeSetConfig.__get_version(config['emc']['version'])
    self.__run_info = CascadeSetConfig.__get_run_info(
      config['run']
    )
    self.__material = CascadeSetConfig.__get_material(
      config['detector']
    )
    self.__main_geometry = CascadeSetConfig.__get_main_geometry(
      config['main_geometry']
    )
    self.__ecuts = CascadeSetConfig.__get_ecuts(
      config
    )

  @property
  def version(self) -> typing.Tuple[int, int, int]:
    """Version, version[0] is major version"""
    return self.__version

  @property
  def run_info(self) -> RunInfo:
    """Contains information about energy and number of cascades."""
    return self.__run_info

  @property
  def material(self) -> Material:
    """Material in which """
    return self.__material

  @property
  def main_geometry(self) -> MainGeometry:
    """Returns main radial geometry."""
    return self.__main_geometry

  @property
  def ecuts(self) -> Cuts:
    """Returns ecuts"""
    return self.__ecuts

  def __repr__(self, *args, **kwargs):
    return (
      "<Config version={self.version} run_info={self.run_info} "
      "material={self.material} cuts={self.ecuts}>"
    ).format(self=self)

  def _validate(self):
    assert self.version == (1, 1, 0), "Only version 1.1.0 is supported."
    assert self.run_info.incidents > 0
    assert self.run_info.energy_mev > 0
    assert self.main_geometry.enabled

  @staticmethod
  def __get_ecuts(config: typing.Mapping[str, dict]):
    def __to_kev(cuts_mev: str):
      return int(float(cuts_mev) * 1000)
    homebrewn_ecuts = __to_kev(config['detector']['cuts_mev'])
    g_ecuts_positon = __to_kev(config['energy_ecuts']['e+_mev'])
    g_ecuts_electron = __to_kev(config['energy_ecuts']['e-_mev'])
    g_ecuts_gamma = __to_kev(config['energy_ecuts']['gamma_mev'])
    assert homebrewn_ecuts < 0, "We don't know how to handle homebrewn ecuts"
    msg = "Cuts must be the same for all particles"
    assert g_ecuts_electron == g_ecuts_positon, msg
    assert g_ecuts_electron == g_ecuts_gamma, msg
    return Cuts(
      type=CutsType.G4Cuts,
      energy_kev=g_ecuts_electron,
      energy_mev="{:.1f}".format(g_ecuts_electron / 1000.0)
    )

  @staticmethod
  def __get_main_geometry(main_geometry: dict) -> MainGeometry:
    return MainGeometry(
      enabled=str_to_bool(main_geometry['enabled']),
      move_z_to_first_interaction=str_to_bool(
        main_geometry['move_z_to_first_interaction']
      ),
      zbins=GeometryCoordinate(
        count=int(main_geometry['z_bins']),
        width_radlen=float(main_geometry['z_bin_width_radlen'])
      ),
      rbins=GeometryCoordinate(
        count=int(main_geometry['r_bins']),
        width_radlen=float(main_geometry['r_bin_width_radlen'])
      ),
    )

  @staticmethod
  def __get_version(version: str) -> typing.Tuple[int, int, int]:
    version_parts = list(map(int, version.split(".")))
    assert len(version_parts) in (2, 3)
    if len(version_parts) == 2:
      version_parts.append(0)
    return tuple(version_parts)

  @staticmethod
  def __get_run_info(run_info: dict):
    return RunInfo(
      particle=InitialParticle.from_config(run_info['particle_type']),
      incidents=int(run_info['incidents']),
      energy_mev=int(run_info['energy_mev'])
    )

  @staticmethod
  def __get_material(detector: dict) -> Material:
    name = detector['material_name']
    return Material(
      name=name,
      name_pretty=MATERIAL_PRETTY_NAMES.get(name.lower(), name),
      w_coeff=float(detector['material_w_coeff']),
      density_g_cm3=float(detector['material_rho_g_cm3']),
      rad_len_mm=float(detector['rad_len_mm'])
    )
