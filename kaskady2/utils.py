"""
Misc utils
"""

import numpy as np

__all__ = [
  "make_readonly_array", "fix_sigma", "get_fwhm"
]


def make_readonly_array(array: np.ndarray, make_view=False) -> np.ndarray:
  """
  Sets flag for this array that make it readonly.

  >>> foo = make_readonly_array(np.zeros(10))
  >>> foo.flags.writeable
  False
  >>> foo[1] = 5
  Traceback (most recent call last):
  ValueError: assignment destination is read-only
  """
  if make_view:
    array = array.view()
  array.setflags(write=False)
  return array


def fix_sigma(sigma_array: np.ndarray, replacement=1E10) -> np.ndarray:
  """
  Performs some basic cleanups on array used as a sigma when fitting.

  Replaces all values that are: nan, inf or zero with `replacelemnt parameter`.
  :param sigma_array: Array to fix
  :param replacement: Value to vis invalid entries with. By default it is 1E10 which

  """
  sigma = np.copy(sigma_array)
  sigma[(sigma == np.inf) | (np.isnan(sigma)) | (sigma == 0)] = replacement
  return sigma


def get_fwhm(*, ydata, xdata):
  """
  Dumbest possble way to estimate fwhm.

  """
  max_index = np.argmax(ydata)
  max_value = ydata[max_index]
  ydata_tmp = np.abs(ydata - max_value / 2)
  left = np.argmin(ydata_tmp[:max_index])
  right = max_index + np.argmin(ydata_tmp[max_index:])
  return xdata[right] - xdata[left]
