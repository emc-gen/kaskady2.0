"""Test module"""
import configparser
import pathlib
from unittest.case import TestCase

from kaskady2.analyze.profile import GeometryCoordinate
from kaskady2.loader.config import CascadeSetConfig, RunInfo, InitialParticle, MainGeometry, Cuts, \
  CutsType

DATA = pathlib.Path(__file__).absolute().parent / 'data' / 'test_reader'


# pylint: disable=missing-docstring


class TestReader(TestCase):

  maxDiff = None

  def setUp(self):
    metadata = DATA / "metadata.ini"
    self.config_parser = configparser.ConfigParser()
    self.config_parser.read(str(metadata))
    self.reader = CascadeSetConfig.open_validated(config=self.config_parser)

  def test_version(self):
    self.assertEqual(self.reader.version, (1, 1, 0))

  def test_repr(self):
    self.assertEqual(
      repr(self.reader), (
        "<Config version=(1, 1, 0) run_info=RunInfo(particle=<InitialParticle.GAMMA: 'GAMMA'>, "
        "incidents=15000, energy_mev=210) material=Material(name='xenon', name_pretty='Liquid Xe', "
        "w_coeff=5.27249680541111, density_g_cm3=2.169999999999999, "
        "rad_len_mm=39.17401302317393) cuts=Cuts(type=<CutsType.G4Cuts: "
        "'G4Cuts'>, energy_kev=1200, energy_mev='1.2')>"
      ))

  def test_run(self):
    self.assertEqual(
      self.reader.run_info,
      RunInfo(InitialParticle.GAMMA, 15000, 210.0)
    )

  def test_material(self):
    mat = self.reader.material
    self.assertEqual(mat.name, "xenon")
    self.assertAlmostEqual(mat.w_coeff, 5.27249680541111)
    self.assertAlmostEqual(mat.density_g_cm3, 2.169999999999999)
    self.assertAlmostEqual(mat.rad_len_mm, 39.17401302317393)

  def test_geometry(self):
    geom = self.reader.main_geometry
    expected = MainGeometry(
      enabled=True,
      move_z_to_first_interaction=True,
      zbins=GeometryCoordinate(count=700, width_radlen=0.1),
      rbins=GeometryCoordinate(count=1, width_radlen=100.0)
    )
    self.assertEqual(geom, expected)

  def test_cuts(self):

    self.assertEqual(
      self.reader.ecuts,
      Cuts(CutsType.G4Cuts, 1200, '1.2')
    )


class TestReadPath(TestReader):
  def setUp(self):
    self.reader = CascadeSetConfig.open_path(DATA / "metadata.ini")


class TestInitialParticle(TestCase):

  def test_gamma(self):
    self.assertEqual(
      InitialParticle.from_config("GaMMa"),
      InitialParticle.GAMMA
    )

  def test_electron(self):
    self.assertEqual(
      InitialParticle.from_config("ElEctRoN"),
      InitialParticle.ELECTRON
    )

  def test_value_error(self):
    with self.assertRaises(ValueError):
      InitialParticle.from_config("not such particle")





