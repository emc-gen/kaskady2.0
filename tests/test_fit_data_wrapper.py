# pylint: disable=missing-docstring,no-self-use
import unittest

import numpy as np
from numpy.testing import assert_array_almost_equal

from scipy.stats.stats import pearsonr

from kaskady2.analyze.profile_fits import (
  LongitudinalProfileFitData,
  FitDataWrapper, SimpleFitData,
)
from tests.utils import get_example_radial_geometry
from .test_fits import TestFitData


class TestFitDataWrapperFitToBin(TestFitData):

  def setUp(self):
    self.profile = get_example_radial_geometry().get_longitudinal_average_profile()
    self.orig_fit_data = LongitudinalProfileFitData(self.profile)
    self.fit_data = FitDataWrapper(self.orig_fit_data, fit_to_bin=20)

  def test_geometry(self):
    assert_array_almost_equal(
      self.profile.geom_radlen[:20],
      self.fit_data.xdata)

  def test_profile(self):
    assert_array_almost_equal(
      self.profile.deposition_mev[:20], self.fit_data.ydata)

  def test_sigma(self):
    profile_sigma = self.profile.deposition_sigma_mev
    self.assert_sigma(profile_sigma[:20])


class TestFitDataWrapperFitUntil(TestFitData):

  def setUp(self):
    self.orig_fit_data = SimpleFitData(
      np.zeros(100),
      np.ones(100),
      (np.ones(100), True)
    )
    self.expected_fit_until = 90
    self.fit_data = FitDataWrapper(self.orig_fit_data, fit_until_deposition_reaches=0.9)

  def test_fit_until(self):
    self.assertEqual(
      self.fit_data.fit_to_bin,
      self.expected_fit_until
    )

  def test_geometry(self):
    assert_array_almost_equal(
      self.orig_fit_data.xdata[:self.expected_fit_until],
      self.fit_data.xdata)

  def test_profile(self):
    assert_array_almost_equal(
      self.orig_fit_data.ydata[:self.expected_fit_until], self.fit_data.ydata)

  def test_sigma(self):
    profile_sigma = self.orig_fit_data.sigma[0]
    self.assert_sigma(profile_sigma[:self.expected_fit_until])


class TestWrapperNoBounds(unittest.TestCase):
  def setUp(self):
    self.orig_fit_data = SimpleFitData(
      np.zeros(100),
      np.ones(100),
      (np.ones(100), False)
    )
    self.fit_data = FitDataWrapper(self.orig_fit_data)

  def test_geometry(self):
    self.assertIs(self.fit_data.xdata, self.orig_fit_data.xdata)

  def test_fit_until(self):
    self.assertIsNone(self.fit_data.fit_to_bin)

  def test_sigma(self):
    assert_array_almost_equal(
      self.fit_data.sigma[0], self.orig_fit_data.sigma[0]
    )
    self.assertEqual(
      self.fit_data.sigma[1],
      self.orig_fit_data.sigma[1]
    )

  def test_profile(self):
    assert_array_almost_equal(
      self.fit_data.ydata, self.orig_fit_data.ydata
    )


class TestFitDataWrapperNormalize(unittest.TestCase):

  NORMALIZE_SUM_TO = 10

  def setUp(self):
    self.orig_fit_data = SimpleFitData(
      np.zeros(100),
      np.linspace(1, 100, 100),
      (np.linspace(1, 10, 100), False)
    )

    self.wrapped_fit_data = FitDataWrapper(
      self.orig_fit_data,
      normalize_sum_to=self.NORMALIZE_SUM_TO
    )

  def test_xdata(self):
    if self.wrapped_fit_data.fit_to_bin is None:
      assert_array_almost_equal(
        self.orig_fit_data.xdata,
        self.wrapped_fit_data.xdata
      )
    else:
      assert_array_almost_equal(
        self.orig_fit_data.xdata[:self.wrapped_fit_data.fit_to_bin],
        self.wrapped_fit_data.xdata
      )

  def test_ydata(self):
    if self.wrapped_fit_data.fit_to_bin is None:
      orig_ydata = self.orig_fit_data.ydata
    else:
      orig_ydata = self.orig_fit_data.ydata[:self.wrapped_fit_data.fit_to_bin]
    ydata_sum = self.wrapped_fit_data.ydata.sum()
    self.assertAlmostEqual(
      ydata_sum,
      self.NORMALIZE_SUM_TO,
    )
    # If ydata is scaled correlation coeff should be 1
    p_coeff, __ = pearsonr(
      orig_ydata,
      self.wrapped_fit_data.ydata
    )
    self.assertAlmostEqual(p_coeff, 1)

  def test_sigma_is_tuple(self):
    self.assertIsInstance(
      self.wrapped_fit_data.sigma,
      tuple
    )
    self.assertIsInstance(
      self.wrapped_fit_data.sigma[0],
      np.ndarray
    )
    self.assertIsInstance(
      self.wrapped_fit_data.sigma[1],
      bool
    )

  def test_sigma(self):
    if self.wrapped_fit_data.fit_to_bin is None:
      orig_sigma = self.orig_fit_data.sigma[0]
    else:
      orig_sigma = \
        self.orig_fit_data.sigma[0][:self.wrapped_fit_data.fit_to_bin]
    # If ydata is scaled correlation coeff should be 1
    p_coeff, __ = pearsonr(
      orig_sigma,
      self.wrapped_fit_data.sigma[0]
    )
    self.assertAlmostEqual(p_coeff, 1)


class TestFitDataWrapperNormTo1(TestFitDataWrapperNormalize):
  """
  The same test as above, but normalize to 1 instead of 10.
  """

  NORMALIZE_SUM_TO = 1




