# pylint: disable=missing-docstring,no-self-use
import collections
import typing
import unittest

import numpy as np
from numpy.testing import assert_array_almost_equal

from kaskady2.analyze.fits import FitObject, ParamEstimations
from kaskady2.analyze.fits.api import FitFunction, InvalidInitialParams
from kaskady2.analyze.profile import LongitudinalProfile
from kaskady2.analyze.profile_fits import (
  LongitudinalProfileFitData,
  LongitudinalProfileFitFunction,
  LongitudinalProfileFitDataProportionalSigma, SimpleFitData,
)
from kaskady2.utils import make_readonly_array, fix_sigma
from tests.utils import get_example_radial_geometry


class TestFitData(unittest.TestCase):

  def setUp(self):
    self.profile = get_example_radial_geometry().get_longitudinal_average_profile()
    self.fit_data = LongitudinalProfileFitData(self.profile)

  def test_geometry(self):
    assert_array_almost_equal(
      self.profile.geom_radlen,
      self.fit_data.xdata)

  def test_profile(self):
    assert_array_almost_equal(
      self.profile.deposition_mev, self.fit_data.ydata)

  def assert_sigma(self, profile_sigma):
    fit_sigma = self.fit_data.sigma[0]

    self.assertIsInstance(self.fit_data.sigma, tuple)
    self.assertIsInstance(fit_sigma, np.ndarray)

    sigma_ok = (
      (profile_sigma != 0) &
      (profile_sigma != np.inf) &
      (np.invert(np.isnan(profile_sigma)))
    )
    assert_array_almost_equal(
      profile_sigma[sigma_ok], fit_sigma[sigma_ok]
    )
    assert fit_sigma is not None
    assert self.fit_data.sigma[1] is True
    assert np.all(fit_sigma != np.nan)
    assert np.all(fit_sigma != np.inf)

  def test_sigma(self):
    profile_sigma = self.profile.deposition_sigma_mev
    self.assert_sigma(profile_sigma)


class TestSimpleFitData(unittest.TestCase):

  def setUp(self):
    self.marker = np.zeros(100)
    self.other = np.zeros(100)
    self.sigma = np.ones(100)

  def test_xdata(self):
    data = SimpleFitData(self.marker, self.other, (self.sigma, True))
    self.assertIs(data.xdata, self.marker)

  def test_ydata(self):
    data = SimpleFitData(self.other, self.marker, (self.sigma, True))
    self.assertIs(data.ydata, self.marker)

  def test_sigma(self):
    data = SimpleFitData(self.other, self.other, (self.sigma, True))
    self.assertIs(data.sigma[0], self.sigma)

  def test_invalid_shape_1(self):
    with self.assertRaises(AssertionError):
      data = np.zeros((10, 10))
      SimpleFitData(data, data, (data, True))

  def test_invalid_shape_2(self):
    with self.assertRaises(AssertionError):
      SimpleFitData(self.marker, np.zeros(10), (self.marker, True))

  def test_invalid_shape_3(self):
    with self.assertRaises(AssertionError):
      SimpleFitData(self.marker, self.marker, (np.zeros(10), True))

  def test_invalid_shape_4(self):
    with self.assertRaises(AssertionError):
      SimpleFitData(self.marker, self.marker, (self.marker, None))

  def test_invalid_sigma(self):
    with self.assertRaises(AssertionError):
      SimpleFitData(self.other, self.other, (np.zeros_like(self.other), True))


class TestFitDataSyntheticSigma(TestFitData):

  def setUp(self):
    self.profile = get_example_radial_geometry().get_longitudinal_average_profile()
    self.fit_data = LongitudinalProfileFitDataProportionalSigma(self.profile)

  def test_sigma(self):
    expected = np.sqrt(self.fit_data.ydata) * 0.03
    expected_ok = self.fit_data.ydata != 0
    assert_array_almost_equal(
      expected[expected_ok],
      self.fit_data.sigma[0][expected_ok]
    )


class TestFitDataSigma(unittest.TestCase):

  def setUp(self):
    sigmas = make_readonly_array(
      np.asarray([1, 2, 3, 0, np.inf, 0, np.nan, 1, 2, 3], dtype=np.float64)
    )
    self.profile = LongitudinalProfile(
      geom_radlen=make_readonly_array(np.ones_like(sigmas)),
      deposition_mev=make_readonly_array(np.ones_like(sigmas)),
      deposition_sigma_mev=sigmas
    )
    self.fit_data = LongitudinalProfileFitData(self.profile)

  def test_sigma_value(self):
    expected = [1, 2, 3, 1E10, 1E10, 1E10, 1E10, 1, 2, 3]
    assert_array_almost_equal(self.fit_data.sigma[0], expected)


class TestProfileFunction(unittest.TestCase):

  def setUp(self):
    self.profile = get_example_radial_geometry().get_longitudinal_average_profile()
    self.fit_data = LongitudinalProfileFitData(self.profile)
    self.fit_func = LongitudinalProfileFitFunction()

  def test_initial_params_are_normalized(self):
    initial_pars = self.fit_func.estimate_initial_parameters(
      self.fit_data.xdata, self.fit_data.ydata
    )

    expected_sum = np.sum(self.fit_data.ydata)
    actual_sum = np.sum(self.fit_func.fit_function(self.fit_data.xdata, *initial_pars.params))

    self.assertAlmostEqual(expected_sum, actual_sum)


class TestDoFitLongitudinalProfile(unittest.TestCase):

  def setUp(self):
    self.profile = get_example_radial_geometry().get_longitudinal_average_profile()
    self.fit_data = LongitudinalProfileFitData(self.profile)
    self.fit_func = LongitudinalProfileFitFunction()
    self.fit_object = FitObject(self.fit_data, self.fit_func)

  def test_do_fit_smokescreen(self):
    self.fit_object.do_fit()

  def test_empty_param_bounds(self):
    initial = self.fit_func.estimate_initial_parameters(
      self.fit_data.xdata, self.fit_data.ydata
    )
    self.fit_func.estimate_initial_parameters = lambda *args, **kwargs: ParamEstimations(
      initial[0], None
    )
    self.fit_object.do_fit()


BrainDeadFitData = collections.namedtuple(
  "BrainDeadFitData", ['xdata', 'ydata', 'sigma'])


class TestFitObjectAssertions(unittest.TestCase):

  def test_sigma(self):
    data = BrainDeadFitData(
      xdata=np.ones(10),
      ydata=np.zeros(10),
      sigma=(np.zeros(10), True)
    )
    fit = FitObject(data, LongitudinalProfileFitFunction())
    with self.assertRaises(AssertionError):
      fit.do_fit()


class TestFixSigma(unittest.TestCase):

  def test_nan(self):
    array = np.arange(1, 10.0)
    array[5] = np.nan
    expected = np.arange(1, 10.0)
    expected[5] = 1E10
    assert_array_almost_equal(
      fix_sigma(array),
      expected
    )

  def test_zero(self):
    array = np.arange(10.0)
    expected = np.arange(10.0)
    expected[0] = 1E10
    assert_array_almost_equal(
      fix_sigma(array),
      expected
    )

  def test_inf(self):
    array = np.arange(1.0, 10.0)
    array[3] = np.inf
    expected = np.arange(1.0, 10.0)
    expected[3] = 1E10
    assert_array_almost_equal(
      fix_sigma(array),
      expected
    )

  def test_other_replacement(self):
    array = np.arange(1.0, 10.0)
    array[3] = np.inf
    expected = np.arange(1.0, 10.0)
    expected[3] = -1
    assert_array_almost_equal(
      fix_sigma(array, replacement=-1),
      expected
    )


class DumbFitFunction(FitFunction):

  def __init__(self, pars, bounds):
    self.pars = pars
    self.bounds = bounds

  def _estimate_initial_parameters(self, xdata: np.ndarray, ydata: np.ndarray) -> ParamEstimations:
    return ParamEstimations(self.pars, self.bounds)

  def fit_function(self, x: np.ndarray, *pars: float) -> np.ndarray:
    raise ValueError()

  def get_param_names(self) -> typing.List[str]:
    return ['a', 'b', 'c']


class TestEstimationsCheck(unittest.TestCase):

  def test_no_bounds(self):
    pars = [1, 2, 3]
    bounds = None
    func = DumbFitFunction(pars, bounds)
    self.assertEqual(
      func.estimate_initial_parameters(None, None),
      ParamEstimations(pars, bounds)
    )

  def test_valid_bounds(self):
    pars = [1, 2, 3]
    bounds = [
      (0, np.inf),
      (0, np.inf),
      (0, np.inf),
    ]
    func = DumbFitFunction(pars, bounds)
    self.assertEqual(
      func.estimate_initial_parameters(None, None),
      ParamEstimations(pars, bounds)
    )

  def test_invalid_bounds_1(self):
    pars = [-11, 2, 3]
    bounds = [
      (0, np.inf),
      (0, np.inf),
      (0, np.inf),
    ]
    func = DumbFitFunction(pars, bounds)
    with self.assertRaises(InvalidInitialParams):
      func.estimate_initial_parameters(None, None)

  def test_invalid_bounds_2(self):
    pars = [1, 2, -3]
    bounds = [
      (0, np.inf),
      (0, np.inf),
      (0, np.inf),
    ]
    func = DumbFitFunction(pars, bounds)
    with self.assertRaises(InvalidInitialParams):
      func.estimate_initial_parameters(None, None)
