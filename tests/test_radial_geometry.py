# pylint: disable=missing-docstring
# pylint: disable=no-self-use
import math
import pathlib
import pickle
import unittest
from tempfile import NamedTemporaryFile

import numpy as np
from numpy.testing import assert_array_almost_equal

from kaskady2.analyze.profile import RadialGeometry, GeometryCoordinate
from kaskady2.utils import make_readonly_array
from tests.utils import get_example_radial_geometry

DATA_DIR = pathlib.Path(__file__).parent / 'data'


class TestRadialGeometry(unittest.TestCase):

  def test_coordinates(self):
    geometry = RadialGeometry(
      make_readonly_array(np.zeros(shape=(10, 1, 10))),
      incidents=10,
      slices=GeometryCoordinate(10, .1),
      cylinders=GeometryCoordinate(1, 100)
    )
    coordinates = geometry.get_profile_coordinates()
    self.assertAlmostEqual(coordinates[0], 0.05)
    self.assertAlmostEqual(coordinates[1], 0.15)

  def test_invalid_array(self):
    """
    Should raise Assertion error when numpy array's shape doesn't match geometry
    """
    with self.assertRaises(AssertionError):
      RadialGeometry(
        np.zeros(shape=(11, 1, 10)),
        incidents=10,
        slices=GeometryCoordinate(10, .1),
        cylinders=GeometryCoordinate(1, 100)
      )

  def test_invalid_shape(self):
    """
    Should raise Assertion error when numpy array's shape doesn't match geometry
    """
    with self.assertRaises(AssertionError):
      RadialGeometry(
        np.zeros(shape=(10, 2, 10)),
        incidents=10,
        slices=GeometryCoordinate(10, .1),
        cylinders=GeometryCoordinate(2, 100)
      )

  def test_get_profile(self):
    data = np.ones(shape=(3, 1, 10))
    data[0] = 1
    data[1] = 2
    data[2] = 3

    geometry = RadialGeometry(
      make_readonly_array(data),
      incidents=3,
      slices=GeometryCoordinate(10, .1),
      cylinders=GeometryCoordinate(1, 100)
    )

    profile = geometry.get_longitudinal_profile(1)

    assert_array_almost_equal(geometry.get_profile_coordinates(), profile.geom_radlen)

    assert_array_almost_equal(profile.deposition_mev, np.full(10, 2.0))

    assert profile.deposition_sigma_mev is None
    assert geometry.incidents == geometry.incident_count

  def test_get_average_profile_fill_with_the_same_data(self):

    geometry = RadialGeometry(
      make_readonly_array(np.full((10, 1, 10), 3.0)),
      incidents=10,
      slices=GeometryCoordinate(10, .1),
      cylinders=GeometryCoordinate(1, 100)
    )

    profile = geometry.get_longitudinal_average_profile()

    assert_array_almost_equal(geometry.get_profile_coordinates(), profile.geom_radlen)
    assert_array_almost_equal(profile.deposition_mev, np.full(10, 3.0))
    assert_array_almost_equal(profile.deposition_sigma_mev, np.zeros(10))

  def test_get_average_profile_fill_with_the_same_different_data(self):
    data = np.ones(shape=(10, 1, 10))

    for ii in range(10):
      data[ii] = ii

    geometry = RadialGeometry(
      make_readonly_array(data),
      incidents=10,
      slices=GeometryCoordinate(10, .1),
      cylinders=GeometryCoordinate(1, 100)
    )

    profile = geometry.get_longitudinal_average_profile()

    assert_array_almost_equal(geometry.get_profile_coordinates(), profile.geom_radlen)

    assert_array_almost_equal(profile.deposition_mev, np.full(10, np.average(np.arange(0, 10))))
    assert_array_almost_equal(
      profile.deposition_sigma_mev, np.full(10, np.std(np.arange(0, 10)) / math.sqrt(10))
    )

  def test_array_from_file(self):
    test = NamedTemporaryFile()
    mmap = np.memmap(test.name, dtype=np.float64, mode='readwrite', shape=(10, 1, 10))
    for ii in range(10):
      mmap[ii] = ii
    mmap.flush()
    geometry = RadialGeometry.from_file(
      pathlib.Path(mmap.filename),
      incidents=10,
      slices=GeometryCoordinate(10, .1),
      cylinders=GeometryCoordinate(1, 100)
    )
    assert_array_almost_equal(mmap, geometry.array)


class TestGeometry(unittest.TestCase):

  DATA_FOR_TEST = DATA_DIR / "test_radial_geometry"

  def setUp(self):
    self.file = RadialGeometry.from_file(
      self.DATA_FOR_TEST / 'main_geometry.dat',
      incidents=100,
      slices=GeometryCoordinate(700, .1),
      cylinders=GeometryCoordinate(1, 100)
    )

  def test_profile(self):
    expected = [
      float(row) for row in (self.DATA_FOR_TEST / 'first_incident.txt').read_text().split()
    ]
    assert len(expected) == 700
    assert_array_almost_equal(
      self.file.get_longitudinal_profile(0).deposition_mev, expected)

  def test_average_profile(self):
    expected_profile = [
      float(row) for row in (self.DATA_FOR_TEST / 'average_profile.txt').read_text().split()
    ]
    profile = self.file.get_longitudinal_average_profile()
    assert_array_almost_equal(profile.deposition_mev, expected_profile)
    expected_profile_sigma = [
      float(row) for row in (self.DATA_FOR_TEST / 'average_sigma.txt').read_text().split()
    ]
    assert_array_almost_equal(profile.deposition_sigma_mev, expected_profile_sigma)

  def test_profile_as_array(self):
    profile_array = self.file.get_profiles_as_array()
    self.assertEqual(len(profile_array.shape), 2)
    self.assertEqual(profile_array.shape, (100, 700))
    expected = [
      float(row) for row in (self.DATA_FOR_TEST / 'first_incident.txt').read_text().split()
    ]
    assert_array_almost_equal(profile_array[0], expected)


class TestGeometryPickles(unittest.TestCase):

  def setUp(self):
    self.data = get_example_radial_geometry().get_longitudinal_average_profile()

  def test_pickles(self):
    data = pickle.dumps(self.data)
    pickle.loads(data)

  def test_copy_is_the_same(self):
    copy = pickle.loads(pickle.dumps(self.data))
    for prop in ['geom_radlen', 'deposition_mev', 'deposition_sigma_mev']:
      assert_array_almost_equal(
        getattr(copy, prop),
        getattr(self.data, prop)
      )


