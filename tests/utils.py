# pylint: disable=no-self-use
# pylint: disable=missing-docstring
import pathlib

from kaskady2.analyze.profile import RadialGeometry, GeometryCoordinate


DATA_DIR = pathlib.Path(__file__).parent / 'data' / 'utils'


def get_example_radial_geometry() -> RadialGeometry:
  return RadialGeometry.from_file(
    DATA_DIR / "main_geometry.dat", incidents=1000, slices=GeometryCoordinate(700, .1),
    cylinders=GeometryCoordinate(1, 100)
  )


