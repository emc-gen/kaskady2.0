
from paver.easy import *


@task
def enable_warnings():
  import warnings
  warnings.simplefilter("error")
  warnings.filterwarnings(
      'ignore', module="paver"
  )
  warnings.filterwarnings(
    'ignore', module="pudb"
  )

@task
@needs(
  ['enable_warnings']
)
def quality():
  sh('pep8 kaskady2 tests')
  sh('pylint kaskady2 tests -r n')



@task
@needs(
  ['enable_warnings']
)
def test_only():
  sh(
    "py.test --cov kaskady2 --cov-report term:skip-covered --cov-report html:target/cov_html --cov-report annotate:target/cov_annotate --doctest-modules")

@task
@needs(
  ['enable_warnings', 'quality', 'test_only']
)
def test():
  import warnings
  warnings.simplefilter("error")


  sh('coverage report -m')
  sh('''coverage report | grep -i -e "TOTAL.*100%" > /dev/null''')


@task
@needs(
  ['enable_warnings']
)
def ipython():
  sh('export PYTHONPATH=`pwd` && cd notebooks && ipython notebook')
